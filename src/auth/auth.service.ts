import { Injectable, UnauthorizedException } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';
import { JwtAuthService } from './jwt.service';
import * as bcrypt from 'bcryptjs';
import { UserService } from './user.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtAuthService: JwtAuthService,
    private readonly redisService: RedisService,
    private readonly userService: UserService,
  ) {}

  async signUp(username: string, password: string): Promise<void> {
    const hashedPassword = await bcrypt.hash(password, 10);
    await this.userService.createUser(username, hashedPassword);
  }

  async signIn(
    username: string,
    password: string,
  ): Promise<{ accessToken: string }> {
    const user = await this.validateUser(username, password);
    if (!user) {
      throw new UnauthorizedException('Invalid credentials');
    }

    const accessToken = await this.jwtAuthService.signToken({ uid: user.id });
    await this.saveTokenInRedis(user.id, accessToken);

    return { accessToken };
  }

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.userService.findByUsername(username);
    if (user && (await this.validatePassword(password, user.password))) {
      return user;
    }
    return null;
  }

  async validatePassword(
    enteredPassword: string,
    hashedPassword: string,
  ): Promise<boolean> {
    return bcrypt.compare(enteredPassword, hashedPassword);
  }

  async saveTokenInRedis(userId: number, accessToken: string): Promise<void> {
    const redisKey = `user:${userId}:token`;
    await this.redisService.getClient().set(redisKey, accessToken);
  }
}
