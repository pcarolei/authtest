import { Repository } from 'typeorm';
import { User } from './user.entity';
import { CustomRepository } from 'src/database/typeorm-ex.decorator';

@CustomRepository(User)
export class UserRepository extends Repository<User> {
  async findByUsername(username: string): Promise<User | null> {
    return this.findOne({ where: { username: username } });
  }
  async createUser(username: string, password: string): Promise<User> {
    const user = this.create({ username, password });
    return this.save(user);
  }
}
