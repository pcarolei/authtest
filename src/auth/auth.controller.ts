import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  Get,
  Param,
  Req,
  HttpException,
  HttpStatus,
} from '@nestjs/common';

import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { JwtAuthService } from './jwt.service';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private userService: UserService,
    private jwtAuthService: JwtAuthService,
  ) {}

  @Post('/signup')
  async signUp(@Body(ValidationPipe) signupData: any): Promise<void> {
    await this.authService.signUp(signupData.username, signupData.password);
  }

  @Post('/signin')
  async signIn(
    @Body(ValidationPipe) signinData: any,
  ): Promise<{ accessToken: string }> {
    return this.authService.signIn(signinData.username, signinData.password);
  }

  @Get('/profile/:username')
  async getProfile(@Req() req: any, @Param('username') username: string) {
    const token = req.headers.authorization?.replace('Bearer ', '');
    if (this.jwtAuthService.verifyToken(token)) {
      const user = await this.userService.findByUsername(username);
      delete user.password;
      return user;
    } else {
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }
  }
}
