import { Module } from '@nestjs/common';
import { TypeOrmExModule } from '../database/typeorm-ex.module';
import { RedisModule } from 'nestjs-redis';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtAuthService } from './jwt.service';
import { UserService } from './user.service';
import { UserRepository } from './user.repository';
import { UserModule } from './user.module';

@Module({
  imports: [
    TypeOrmExModule.forCustomRepository([UserRepository]),
    RedisModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        url: configService.get<string>('REDIS_URL'),
      }),
      inject: [ConfigService],
    }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
        signOptions: { expiresIn: '1h' },
      }),
      inject: [ConfigService],
    }),
    UserModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtAuthService, UserService],
  exports: [AuthService, JwtModule],
})
export class AuthModule {}
